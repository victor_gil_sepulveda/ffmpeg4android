#!/bin/bash

# set the base path to your Android NDK (or export NDK to environment)
if [[ "x$NDK_BASE" == "x" ]]; then
    NDK_BASE=/opt/android-ndk
    echo "No NDK_BASE set, using $NDK_BASE"
fi

NDK_PLATFORM_VERSION=3
NDK_ABI=arm
NDK_COMPILER_VERSION=4.6
NDK_SYSROOT=$NDK_BASE/platforms/android-$NDK_PLATFORM_VERSION/arch-$NDK_ABI
NDK_UNAME=`uname -s | tr '[A-Z]' '[a-z]'`

HOST=$NDK_ABI-linux-androideabi
NDK_TOOLCHAIN_BASE=$NDK_BASE/toolchains/$HOST-$NDK_COMPILER_VERSION/prebuilt/$NDK_UNAME-x86
STRIP=$NDK_TOOLCHAIN_BASE/bin/$NDK_ABI-linux-androideabi-strip
CC="$NDK_TOOLCHAIN_BASE/bin/$HOST-gcc --sysroot=$NDK_SYSROOT"
LD=$NDK_TOOLCHAIN_BASE/bin/$HOST-ld


CWD=`pwd`
PROJECT_ROOT=$CWD
EXTERNAL_ROOT=$PROJECT_ROOT

# install root for built files
DESTDIR=$EXTERNAL_ROOT
prefix=/data/ffmpeg/app_opt
LOCAL=$DESTDIR$prefix

./configure --arch=arm \
--enable-cross-compile \
--target-os=linux \
--enable-pic \
--cross-prefix=$NDK_TOOLCHAIN_BASE/bin/$NDK_ABI-linux-androideabi- \
--sysroot="$NDK_SYSROOT" \
--extra-cflags="-marm -march=armv5" \
--enable-yasm \
--disable-logging \
--fatal-warnings \
--disable-gpl \
--enable-static \
--disable-shared \
--enable-small \
--disable-runtime-cpudetect \
--disable-programs \
--disable-programs \
--enable-ffmpeg \
--disable-doc \
--disable-network \
--disable-everything \
--enable-protocol=file \
--enable-filters \
--enable-encoder=h263 \
--enable-decoder=mjpeg \
--enable-decoder=png \
--enable-decoder=jpegls \
--enable-decoder=jpeg2000 \
--enable-muxers \
--enable-demuxer=image2 \
--disable-hardcoded-tables \
--enable-memalign-hack

make clean

make -j4

mv ffmpeg ffmpeg_armv5