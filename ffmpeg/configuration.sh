#!/bin/bash
echo "Configuring..."


./configure --disable-logging \
    --fatal-warnings \
    --disable-gpl \
    --enable-static \
    --disable-shared \
    --enable-small \
    --disable-runtime-cpudetect \
    --disable-programs \
    --disable-programs \
    --enable-ffmpeg \
    --disable-doc \
    --disable-network \
    --disable-everything \
    \
    --enable-protocol=file \
    --enable-filters\
    --enable-encoder=h263 \
    --enable-decoder=mjpeg \
    --enable-decoder=png \
    --enable-decoder=jpegls \
    --enable-decoder=jpeg2000 \
    \
    --enable-muxers\
    --enable-demuxer=image2 \
    --disable-hardcoded-tables \
    --enable-memalign-hack

make clean

make -j4

./ffmpeg -y -f image2 -loop 1 -r 12 -vframes 12 -vcodec mjpeg -i flower.jpg -an -vcodec h263 -vb 56k -s 704x576 -r 12 -vframes 12 -vf crop=704:576:0:0  flower.3gp
