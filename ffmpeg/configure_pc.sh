#!/bin/bash
./configure --enable-yasm \
--disable-logging \
--fatal-warnings \
--disable-gpl \
--enable-static \
--disable-shared \
--enable-small \
--disable-runtime-cpudetect \
--disable-programs \
--enable-ffmpeg \
--disable-doc \
--disable-network \
--disable-everything \
--enable-protocol=file \
--enable-filter=crop \
--enable-encoder=h263 \
--enable-decoder=mjpeg \
--enable-decoder=png \
--enable-decoder=jpegls \
--enable-decoder=jpeg2000 \
--enable-muxer=h263 \
--enable-demuxer=image2 \
--disable-hardcoded-tables \
--enable-memalign-hack

make clean

make -j4

mv ffmpeg ffmpeg_pc